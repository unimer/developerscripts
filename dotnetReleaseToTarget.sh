#! /bin/bash
PROJECT="PORTAL"
SOURCE_PATH="/mnt/c/Users/Nikola/Projects/portal/portal/bin/Debug/net6.0"
TARGET_PATH="/mnt/c/VMshare"
DATE=`date +%Y-%m-%d__%H-%M`

FILE_PATH=$TARGET_PATH/$PROJECT-debug-release-$DATE.zip 
echo "Deleting old release files..."
echo ""
rm -rf $TARGET_PATH/$PROJECT*.zip
cd $SOURCE_PATH
echo "Generating release zip..."
echo ""
zip -r $FILE_PATH .
echo "Removing webconfig files..."
zip -d $FILE_PATH "appsettings.json" "appsettings.Development.json"
