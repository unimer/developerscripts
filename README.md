# createBlank.sh

Shell script that creates _**_n_**_ files 

**Usage: **

`createBlank.sh <numberOfFiles> <extension>`

*example*
`createBlank.sh 5 json`

# dotnetReleaseToTarget.sh

Shell script generates zip of the .net release and moves zip file to wanted destination folder.

**Use case 1**:
I was developing a specific feature for customer which had to be tested on customers test stage, because customers SAP instance was only available on that stage. So, each time there is a change in code, I had to zip build manually, copy it to virtual machine and then from virtual machine to customer stage. Then I had to remove default `appsettings.json`. This script speeds up the process. So I just have to move the file from VM to customer stage manually. 

run `./releaseToVM.sh`
